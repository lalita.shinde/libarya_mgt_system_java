package com.sunbeaminfo.LibraryManagement.utils;

public class Constants 
{
	
	public static final String ROLE_OWNER = "OWNER";
	
	public static final String ROLE_LIBRARIAN = "LIBRARIAN";
	public static final String ROLE_MEMBER = "MEMBER";
	 
	public static final String STATUS_AVAILABLE="Available";
	public static final String STATUS_NOT_AVAILABLE="Not Available";
	 
	public static final String TYPE_FEES = "Fees";
	public static final String TYPE_FINE = "Fine";
	public static final String FINE_AMOUNT = "5";


	public static final long MEMBERSHIP_DAYS = 30;
	public static final long RETURN_DAYS = 7;

}
